FROM openjdk:8
COPY . /usr/myapp
WORKDIR /usr/myapp
RUN javac Main.java
CMD ["java", "Main"]