package com.mercadolibre.Data.Test;

import com.google.gson.JsonObject;
import com.mercadolibre.Data.Repository.Ip2Country;
import org.junit.Assert;
import org.junit.Test;

public class Ip2CountryTest {

    @Test
    public void makeTest() throws Exception {
        Ip2Country ip2Country = new Ip2Country("24.232.136.5");
        JsonObject json = ip2Country.make();
        Assert.assertEquals(json.getClass(), JsonObject.class);
    }

    @Test (expected = Exception.class)
    public void makeTest2() throws Exception {
        Ip2Country ip2Country = new Ip2Country("");
        JsonObject json = ip2Country.make();
    }

}
