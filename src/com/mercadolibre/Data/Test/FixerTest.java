package com.mercadolibre.Data.Test;

import com.google.gson.JsonObject;
import com.mercadolibre.Data.Repository.Fixer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FixerTest {

    private Fixer fixer;

    @Before
    public void before() {
        fixer = new Fixer();
    }

    @Test
    public void makeTest() throws Exception {
        JsonObject json = fixer.make();
        Assert.assertEquals(json.getClass(), JsonObject.class);
    }

}
