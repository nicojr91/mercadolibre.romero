package com.mercadolibre.Data.Test;

import com.google.gson.JsonObject;
import com.mercadolibre.Data.Repository.RestCountries;
import org.junit.Assert;
import org.junit.Test;

public class RestCountriesTest {

    @Test
    public void makeTest() throws Exception {
        RestCountries restCountries = new RestCountries("ARG");
        JsonObject json = restCountries.make();
        Assert.assertEquals(json.getClass(), JsonObject.class);
    }


    @Test (expected = Exception.class)
    public void makeTest2() throws Exception {
        RestCountries restCountries = new RestCountries("");
        JsonObject json = restCountries.make();
    }

}
