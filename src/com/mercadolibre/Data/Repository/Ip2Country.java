package com.mercadolibre.Data.Repository;

import com.google.gson.JsonObject;
import com.mercadolibre.Domain.Repository.Request;

public class Ip2Country extends Request {

    public Ip2Country(String ip) {
        super("https://api.ip2country.info/ip?" + ip);
    }

    @Override
    public JsonObject make() throws Exception {
        return this.callApi();
    }
}
