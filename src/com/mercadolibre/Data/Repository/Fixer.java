package com.mercadolibre.Data.Repository;

import com.google.gson.JsonObject;
import com.mercadolibre.Domain.Repository.Request;

public class Fixer extends Request {

    public Fixer() {
        super("http://data.fixer.io/api/latest?access_key=d9a3a926464f3f3bbf93fc15d8fbc11d");
    }

    @Override
    public JsonObject make() throws Exception {
        return callApi();
    }

}
