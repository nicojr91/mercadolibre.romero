package com.mercadolibre.Data.Repository;

import com.google.gson.JsonObject;
import com.mercadolibre.Domain.Repository.Request;

public class RestCountries extends Request {

    public RestCountries(String isoCode) {
        super("https://restcountries.eu/rest/v2/alpha/" + isoCode);
    }

    @Override
    public JsonObject make() throws Exception {
        return this.callApi();
    }
}
