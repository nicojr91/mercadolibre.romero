package com.mercadolibre.App.Test;

import com.mercadolibre.App.Controller.CountryController;
import com.mercadolibre.Domain.Entity.Country;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CountryControllerTest {

    private CountryController controller;

    @Before
    public void Before() {
        controller = new CountryController();
    }

    @Test
    public void searchTest() throws Exception {
        Country country = controller.Search("5.6.7.8");
        Assert.assertEquals("Germany", country.getName());
    }

    @Test (expected = Exception.class)
    public void InvalidIPTest() throws Exception {
        Country country = controller.Search("56.7.8");
    }

    @Test (expected = Exception.class)
    public void unknownCountryTest() throws Exception {
        Country country = controller.Search("255.255.255.255");
    }
}
