package com.mercadolibre.App;

import com.mercadolibre.App.Controller.CountryController;
import com.mercadolibre.Domain.Entity.Country;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {

    private static CountryController controller;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        controller = new CountryController();

        String input = getInput(scanner);
        while(!input.equals("0")) {
            showDataByIP(input);

            input = getInput(scanner);
        }
    }

    private static void showDataByIP(String ip) {
        try {
            Date date_start = new Date();
            Country country = controller.Search(ip);
            if(country == null) {
                System.out.println("Whoops, something was wrong. Try again with other IP.");
                return;
            }
            System.out.println("-----------------------------------------------------------");
            System.out.println("IP: " + ip + ", Fecha actual: " + (new SimpleDateFormat("dd/MM/Y HH:mm:ss")).format(new Date()));
            System.out.println("    Pais: " + country.getName());
            System.out.println("    ISO Code: " + country.getIsoCode());
            System.out.println("    Idiomas: ");
            for (Object lang: country.getLanguages()) {
                System.out.println("        - " + lang.toString());
            }
            System.out.println("    Moneda: " + country.getCurrency() + "(1 U$D = " +  ((new DecimalFormat("0.0000").format(country.getExchange().getValue()))) + ")");
            System.out.println("    Hora: ");
            for (Object timezone: country.getTimezones()) {
                System.out.println("        - " + getTimeByTimeZone(timezone.toString()) + "(" + timezone.toString() + ")");
            }
            System.out.println("    Distancia estimada: " + country.distanceOfBuenosAires() + "KM (" + country.getLatBS() + ", " + country.getLngBS() + ") a ("+country.getLatitud()+", "+country.getLongitud()+")");
            Date date_end = new Date();
            System.out.println( (date_end.getTime() - date_start.getTime() )+ "ms");
            System.out.println("-----------------------------------------------------------\n");
        } catch (Exception e) {
            System.out.println("Whoops, something was wrong.");
        }
    }

    private static String getInput(Scanner scanner) {
        System.out.println("Utilice 0 para salir");
        System.out.print("Ingrese una IP: ");

        return scanner.nextLine().trim();
    }

    private static String getTimeByTimeZone(String timezone) {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timeZone);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        simpleDateFormat.setTimeZone(timeZone);
        int hours = Integer.parseInt(timezone.substring(timezone.indexOf('C')+1, timezone.indexOf(':')));
        calendar.add(Calendar.HOUR, hours);

        return simpleDateFormat.format(calendar.getTime());
    }
}
