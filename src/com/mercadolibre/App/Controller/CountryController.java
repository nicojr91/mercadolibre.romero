package com.mercadolibre.App.Controller;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mercadolibre.Data.Repository.Fixer;
import com.mercadolibre.Data.Repository.Ip2Country;
import com.mercadolibre.Data.Repository.RestCountries;
import com.mercadolibre.Domain.Entity.Country;
import com.mercadolibre.Domain.Entity.Exchange;
import com.mercadolibre.Domain.Repository.Log;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class CountryController {

    private LoadingCache<String, Map<String, Exchange>> cacheCurrencies;
    private LoadingCache<String, Country> cacheCountries;

    private static final Pattern PATTERN = Pattern.compile("^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

    public CountryController() {
        loadCaches();
    }

    public Country Search(String ip) throws Exception {
        Log log = Log.getInstance();
        if(!validate(ip)) {
            log.getLogManager().error("Invalid IP.");
            throw new Exception("Invalid IP.");
        }

        JsonObject dataReq;
        Ip2Country ip2Country = new Ip2Country(ip);
        dataReq = ip2Country.make();
        if(dataReq.get("countryName").getAsString().equals("")) {
            log.getLogManager().error("No country contains that IP.");
            throw new Exception("No country contains that IP.");
        }

        String isoCode = dataReq.get("countryCode3").getAsString();
        Country c;
        c = cacheCountries.get(isoCode);

        return c;
    }

    private Country createCountry(String isoCode) throws Exception {
        JsonObject dataReq;
        RestCountries rest = new RestCountries(isoCode);
        dataReq = rest.make();
        Country country = new Country(dataReq.get("name").getAsString(), dataReq.get("alpha3Code").getAsString());
        country.setCurrency(((JsonObject)((JsonArray)dataReq.get("currencies")).get(0)).get("code").getAsString());
        for(JsonElement elem: (JsonArray)dataReq.get("languages")) {
            country.addLanguage(((JsonObject) elem).get("nativeName").getAsString());
        }
        for(JsonElement elem: (JsonArray)dataReq.get("timezones")) {
            country.addTimezone(elem.getAsString());
        }

        country.setLatitud(((JsonArray)dataReq.get("latlng")).get(0).getAsDouble());
        country.setLongitud(((JsonArray)dataReq.get("latlng")).get(1).getAsDouble());

        //no se esta pudiendo hacer la consulta porque es en Request HTTPS
        Map<String, Exchange> map = cacheCurrencies.get("exchanges");
        if(map != null) {
            country.setExchange(map.get(country.getCurrency()));
        }

        return country;
    }

    private boolean validate(String ip) {
        return PATTERN.matcher(ip).matches();
    }

    private Map<String, Float> toJson(String json) {
        Type type = new TypeToken<Map<String, Float>>(){}.getType();

        return new Gson().fromJson(json, type);
    }

    private void loadCaches() {
        //CACHE CURRENCIES
        CacheLoader<String, Map<String, Exchange>> loaderCurrencies;
        loaderCurrencies = new CacheLoader<String, Map<String, Exchange>>() {
            @Override
            public Map<String, Exchange> load(String s) throws Exception {
                Fixer fixer = new Fixer();
                JsonObject obj = fixer.make();
                if(obj.get("success").getAsBoolean()) {
                    Map<String, Exchange> mapExchanges = new HashMap<String, Exchange>();
                    Map<String, Float> map = toJson(obj.get("rates").toString());
                    for (String key: map.keySet()) {
                        float value = map.get(key) * (map.get("EUR") / map.get("USD"));
                        Exchange exchange = new Exchange(key, value);
                        mapExchanges.put(key, exchange);
                    }
                    return mapExchanges;
                }

                return null;
            }
        };
        cacheCurrencies = CacheBuilder.newBuilder().expireAfterWrite(2, TimeUnit.HOURS).build(loaderCurrencies);

        //CACHE COUNTRIES
        CacheLoader<String, Country> loaderCountries;
        loaderCountries = new CacheLoader<String, Country>() {
            @Override
            public Country load(String isoCode) throws Exception {
                return createCountry(isoCode);
            }
        };
        cacheCountries = CacheBuilder.newBuilder().expireAfterWrite(2, TimeUnit.HOURS).build(loaderCountries);
    }
}
