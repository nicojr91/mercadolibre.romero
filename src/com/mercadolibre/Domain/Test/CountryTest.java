package com.mercadolibre.Domain.Test;

import com.mercadolibre.Domain.Entity.Country;
import org.junit.Assert;
import org.junit.Test;

public class CountryTest {

    @Test
    public void testCountry() {
        Country country = new Country("Argentina", "ARG");
        country.setLatitud(-34);
        country.setLongitud(-64);
        country.setCurrency("ARS");
        country.addTimezone("UTC-03:00");
        country.addLanguage("Español");
        country.addLanguage("Guaraní");

        Assert.assertEquals(country.getName(), "Argentina");
        Assert.assertEquals(country.getIsoCode(), "ARG");
        Assert.assertEquals(country.getCurrency(), "ARS");
    }

}
