package com.mercadolibre.Domain.Repository;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import javax.net.ssl.*;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public abstract class Request {

    /**
     * El nombre y código ISO del país --> https://api.ip2country.info/ip?5.6.7.8
     * Los idiomas oficiales del país --> https://restcountries.eu/rest/v2/alpha/col
     * Hora(s) actual(es) en el país (si el país cubre más de una zona horaria, mostrar todas) --> https://restcountries.eu/rest/v2/alpha/col
     * Distancia estimada entre Buenos Aires y el país, en km.
     * Moneda local, y su cotización actual en dólares (si está disponible) --> http://data.fixer.io/api/latest?access_key=d9a3a926464f3f3bbf93fc15d8fbc11d&format=1
     */

    private String apiUrl;

    public Request(String url) {
        this.apiUrl = url;
    }

    public abstract JsonObject make() throws Exception;

    protected JsonObject callApi() throws Exception {
        try {
            HttpRequest request = HttpRequest.newBuilder(new URI(apiUrl)).GET().build();
            HttpResponse<String> response = HttpClient.newBuilder().build().send(request, HttpResponse.BodyHandlers.ofString());

            if(response.statusCode() != 200) {
                Log.getInstance().getLogManager().error("Reponse Code: " + response.statusCode());
                throw new Exception(response.uri().toString() + " - Code: " + response.statusCode() );
            }

            return this.toJson(response.body());
        } catch (SSLHandshakeException e) {
            Log.getInstance().getLogManager().error(e.getMessage());
        }

        return null;
    }

    private JsonObject toJson(String json) {
        Type type = new TypeToken<JsonObject>(){}.getType();

        return new Gson().fromJson(json, type);
    }

}
