package com.mercadolibre.Domain.Repository;

import com.mercadolibre.App.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log {
    private static Log ourInstance;

    public static Log getInstance() {
        if(ourInstance == null) {
            ourInstance = new Log();
        }

        return ourInstance;
    }

    private Logger logManager = LogManager.getLogger(Main.class);

    private Log() { }

    public Logger getLogManager() {
        return logManager;
    }
}
