package com.mercadolibre.Domain.Entity;

import java.util.ArrayList;

import static java.lang.Math.round;

public class Country {

    private String name;
    private String isoCode;
    private String currency;
    private ArrayList<String> languages;
    private ArrayList<String> timezones;
    private double latitud;
    private double longitud;
    private double latBS;
    private double lngBS;
    private Exchange exchange;

    public Country(String name, String isoCode) {
        this.name = name;
        this.isoCode = isoCode;
        this.currency = "";
        this.languages = new ArrayList<>();
        this.timezones = new ArrayList<>();
        latBS = -34;
        lngBS = -64;
    }


    /* SETTER & GETTERS */

    public void setCurrency(String currency) {
    this.currency = currency;
}

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public double getLatBS() {
        return latBS;
    }

    public double getLngBS() {
        return lngBS;
    }

    public String getName() {
        return name;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public String getCurrency() {
        return currency;
    }

    public ArrayList getTimezones() {
        return timezones;
    }

    public ArrayList getLanguages() {
        return languages;
    }

    public void addLanguage(String language) {
        this.languages.add(language);
    }

    public void addTimezone(String timezone) {
        this.timezones.add(timezone);
    }

    public int distanceOfBuenosAires() {
        if ((latBS == latitud) && (lngBS == longitud)) {
            return 0;
        }

        double theta = lngBS - longitud;
        double dist = Math.sin(Math.toRadians(latBS)) * Math.sin(Math.toRadians(longitud)) + Math.cos(Math.toRadians(latBS)) * Math.cos(Math.toRadians(longitud)) * Math.cos(Math.toRadians(theta));
        dist = Math.acos(dist);
        dist = Math.toDegrees(dist);
        dist = dist * 60 * 1.1515 * 1.609344;

        return (int)round(dist);
    }

    public Exchange getExchange() {
        return exchange;
    }

    public void setExchange(Exchange exchange) {

        this.exchange = exchange;
    }
}
