package com.mercadolibre.Domain.Entity;

public class Exchange {
    private String currency;
    private float value;

    public Exchange() {}

    public Exchange(String currency, float value) {
        this.value = value;
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
